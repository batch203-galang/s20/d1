// console.log("Hello World");

// Repetition Control Structure (Loops)
	// Loops are one of the most important feature that programming must have.
	// It lets us execute code repeatedly in a pre-set number or maybe forever.

/*
	


*/

function greeting() {
	console.log("Hello My World!");
}

// greeting();
// greeting();
// greeting();
// greeting();
// greeting();
// greeting();
// greeting();
// greeting();
// greeting();
// greeting();

// let count = 10;

// while(count !== 0){
// 	console.log("This is printed inside the loop: " +count);
// 	greeting();

// 	// iteration
// 	count --;
// }

// [SECTION] While Loop
	// While loop allow us to repeat an action or an instruction as long as the condition is true.

	/*
		Syntax:
			while(expression/condition){
				//statement/code block

				finalExpression ++/-- (iteration)
			}

			-expression/condition => This are the unit of code that is being evaluated in our loop. Our loop will run while the condition/expression is true.
			- statement/code block => cond/instructions that will be executed several times.
			- finalExpression => indicates how to advance the loop.
	*/


	// let count = 5;

	// //while the value of count is not equal to 0.
	// while(count !==0){
		
	// 	console.log("Current value of count in the condition: " + count + " !==0 is "+(count!==0));

	// 	//console.log("While: "+count);

	// 	//Forgetting to include this in loop will make our application run an infinite loop which will eventually crash our devices.
	// 	// Decreases the value of count by 1 after every iteration of our loop to stop it when it reaches to 0.
	// 	count--;
	// }

// [SECTION] Do While Loop
	// A Do While loop works a lot like the while loop


	/*
		Syntax:
			do{
				//statement/code block

				finalExpression ++/--
			} while (expression/condition)
	*/

			// Number() can also convert a boolean or date.
//let number = Number(prompt("Give me a number"));

// do {
// 	console.log("Do While: " +number);

//	//increase the value of number by 1 after every iteration to stop when reaches 10 or greater.
// 	number += 1;
//	//Providing a number of 10 or greater will run the code block once and will stop the loop
// } while (number < 10)

// while(number < 10){
// 	console.log("While :" +number);

// 	number++
//}

// [SECTION] For loop
	// A for loop is more flexible than while and do-while loops.

	/*
		Syntax:
			for(initialization; expression/condition; finalExpression++/--){
				//statement/code block
			}

			Three parts of the for loop:
			1. initialization => value that will track the progression of the loop.
			2. expression/condition => this will be evaluated to determine if the loop will run one more time.
			3. finalExpression => indicates how the loop advances.


	*/

	// for(let count = 0; count <= 20; count++){ //or you can change count to count+=2
	// 	if(count%2 == 0 && count !== 0){
	// 		console.log("For loop: "+count);
	// 	}
	// }

	/*
		let myString = "ruth"
		
		expected output:
		r
		u
		t
		h

	*/

	// Loop using String property

	//let myString = "ruth";

	// .length property is used to count the number of characters in a string.
	//console.log(myString.length);

	// Accessing element of a string
	// Individual characters of a string may be accessed using its index number.
	// The first character in string corresponds to the number 0, to the nth number.

	//console.log(myString[0]); //to access the starting element
	//console.log(myString[3]); //h
	//console.log(myString[myString.length-1]);//to access the last element of the string

	//Create the loop that will display
	//for(let x=0; x < myString.length; x++){
	//	console.log(myString[x]);
	//}

	/*
		Create a loop that will prinout the letters of the name individually and print out the number "3" instead when the letter to be printed out is vowel.

	*/

	let myName = "Ruth"; //expected: r 3 t h
	let myNewName = "";

// 	for (let i = 0; i < myName.length; i++){
// 		//console.log(myName[i].toLowerCase());

// 		//if the character of your name is a vowel letter, instead of a character, it will display the number "3"
// 		if(myName[i].toLowerCase() == 'a' || 
// 			myName[i].toLowerCase() == 'e' || 
// 			myName[i].toLowerCase() == 'i' || 
// 			myName[i].toLowerCase() == 'o' || 
// 			myName[i].toLowerCase() == 'u'){
// 			console.log(3);

// 			myNewName += 3;
// 		}
// 		else{
// 			//consonants
// 			console.log(myName[i]);
// 			myNewName += myName[i];
// 		}
// }

// console.log(myNewName);

// [SECTION] Continue and break statements

/*
	- "continue" statement allows the code to go to the next iteration of the loop without finishing the execution of all the code block.
	- "break" statement is used to terminate the current loop once a match has been found.

*/
/*
	Create a loop that if the count values is divided by 2 and the remainder is 0, it will not print the number and continue to the next iteration of the loop. If the count value is greater than 10 it will stop the loop.

*/

	// for(let count = 0; count <= 20; count++){
	// 	//console.log(count);

	// 	if(count % 2 === 0){
	// 		continue;
	// 		//This ignores all the statements located after the continue statement;
	// 		//console.log("Even: "+count);
	// 	}
	// 		console.log("Continue and Break: "+count);
		

	// 	if (count > 10){
	// 		break;
	// 	}
	// }

	/*

		Mini Activty:
		Creates a loop that will iterate based on the length of the string. If the current letter is equivalent to "a", print "Continue to the next iteration" and skip the current letter. If the current letter is equal to "d" stop the loop.

		let name = "Alexandro";

	*/


	let name = "Alexandro";

	for (let n = 0; n < name.length; n++){
		//console.log(name[n].toLowerCase());

		if(name[n].toLowerCase() == 'a'){
			console.log("Continue to the next iteration.");
		}

		else{
			console.log(name[n]);
		}

		if(name[n].toLowerCase() == 'd'){
			break;
		}
	}

